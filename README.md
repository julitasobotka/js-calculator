# Solution: JS Calculator

## Notes

1. I slightly changed the styling of the calculator to enable two lines of equation on display. Text resizing function is a better idea for future development.
2. A limit on the number of characters in equation should be implemented.
3. There should be a better solution to the problem of JS float precision.