const setButtonsDisabled = (buttons) => {
    buttons.forEach(button => {
        button.setAttribute("disabled", "disabled");
    });
}
const setButtonsEnabled = (buttons) =>{
    buttons.forEach(button => {
        button.removeAttribute("disabled");            
    });
}
const getResult = (equation, operators, operatorFunctions) => {
    let calcArray = [];
    let currentFunction;
     //When minus is first element of equation - add '0' before it
    if(equation[0] == '-'){
        equation.unshift(0);
    } 
    //Loop over operators (for right order of math operations)
    for(i=0; i<operators.length; i++){
        //Loop over elements in equation
        for(j=0; j<equation.length; j++){
            //When current element in equation is current operator
            if (equation[j] == operators[i]) {
                currentFunction = operatorFunctions[i];
            } 
            //When previous element in equation was current operator
            else if (currentFunction) {
                let lastIndex = calcArray.length - 1;
                calcArray[lastIndex] = currentFunction(calcArray[lastIndex], equation[j]);
                currentFunction = null;
            } 
            else {
                calcArray.push(equation[j]);
            }
        }
    equation = calcArray;
    calcArray = [];
    }
    return equation[0];
}
const postCalculatedValue = result => {
    const url = 'calculations.php?action=addcalculation';
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify({ result })
    })
    .then(res => res.json())
    .catch(err => console.error(err));
}
function calculator(){
    const mainDisplay = document.getElementById('mainDisplay');
    const upperDisplay = document.getElementById('upperDisplay');
    const operatorButtons =  Array.from(document.getElementsByClassName("js-operators")); // (+ * /)
    const dotButton = document.getElementById('dotButton');
    const equalButton = document.getElementById('equalButton');
    const saveButton = document.getElementById('saveButton');
    const operators = ['÷', '×', '-', '+'];
    const operatorFunctions = [
        (a, b) => (1000000*a)/(1000000*b),
        (a, b) => ((1000000*a)*(1000000*b))/1000000000000,
        (a, b) => ((1000000*a)-(1000000*b))/1000000,
        (a, b) => ((1000000*a)+(1000000*b))/1000000
    ]; // operator functions with basic solution for float precision 
    let equation = []; //equation to solve
    let current = '0'; //store element before adding to equation

    setButtonsDisabled([...operatorButtons, equalButton, saveButton]); //disable operators that can't be clicked first (+ * / =) and save button

    document.getElementById('numbers').addEventListener('click', (e)=>{
        if (e.target !== e.currentTarget) {
            const clickedChar = e.target.value; 
            //When its first time when digit is clicked - enable operator buttons (+ * /)
            if(equation.length == 0 && clickedChar != '0'){
                setButtonsEnabled(operatorButtons);
            }
            //When dot clicked - disable dot (prevent situation when stored is i.e. '71.10.2')
            if(clickedChar == '.'){
                setButtonsDisabled([dotButton]);
            }
            //When zero stored and digit is clicked - remove 0 (it will be replaced)
            if(current == '0' && clickedChar != '.'){
                mainDisplay.innerHTML = mainDisplay.innerHTML.substring(0, mainDisplay.innerHTML.length - 1);
                current = clickedChar;    
            }
            //When operator stored - add operator to equation, clicked char is stored, dot is enabled
            else if(operators.indexOf(current) > -1){
                equation.push(current);
                current = clickedChar;
                setButtonsEnabled([dotButton]);
            }
            //When number stored - add clicked char to stored
            else{
                current += clickedChar;
            }
            mainDisplay.innerHTML += clickedChar;
            //When its at least one math operation in equation if we add current - enable equal button
            if((equation[0] == '-' && equation.length > 2) || (equation[0] != '-' && equation.length > 1)){
                setButtonsEnabled([equalButton]);
            }
            //Disable save button
            setButtonsDisabled([saveButton]);
        }
        e.stopPropagation();
    });
    document.getElementById('operators').addEventListener('click', (e)=>{
        if (e.target !== e.currentTarget) {
            const clickedChar = e.target.value; 
            //When only first zero or operator stored - remove char (it will be replaced)
            if(operators.indexOf(current) > -1 || (equation.length == 0 && current == '0')){
                mainDisplay.innerHTML = mainDisplay.innerHTML.substring(0, mainDisplay.innerHTML.length - 1);
            }
            //When number stored - add number (as a float) to equation
            else{
                equation.push(parseFloat(current));
            }
            //When equal is clicked - solve equation, change displays, clear equation, disable equal button
            if(clickedChar == '='){
                const result = getResult(equation, operators, operatorFunctions); 
                upperDisplay.innerHTML = mainDisplay.innerHTML+'=';
                mainDisplay.innerHTML = result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                current = result;
                equation = [];  
                setButtonsEnabled([saveButton]);
                setButtonsDisabled([equalButton]);
                if(result == '0'){
                    setButtonsDisabled(operatorButtons);
                }
            }
            //When other operator is clicked - add operator to display, clicked operator is stored, disable dot and save button
            else{
                current = clickedChar;
                mainDisplay.innerHTML += clickedChar;   
                setButtonsDisabled([dotButton, saveButton]); //dot and save disabled after operator
            }
            //When its at least one math operation in equation - enable equal button
            if((equation[0] == '-' && equation.length > 3) || (equation[0] != '-' && equation.length > 2)){
                setButtonsEnabled([equalButton]);
            }
        }
        e.stopPropagation();
    });
    document.getElementById('acButton').addEventListener('click', (e)=>{
        equation = [];
        current = '0';
        mainDisplay.innerHTML = '0';
        upperDisplay.innerHTML = '';
        setButtonsEnabled([dotButton]);
        setButtonsDisabled([...operatorButtons, equalButton, saveButton]);
    });
    document.getElementById('saveButton').addEventListener('click', (e) => {  
        postCalculatedValue(mainDisplay.innerHTML).then( res => console.log(res));
    });
}
calculator();