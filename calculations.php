<?php
    if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
        $action = $_REQUEST['action'];
        switch($action) {
            case 'addcalculation' : 
                $response = array('success' => addCalculation());
                echo json_encode($response);            
                break;
        }
    }
    else {
        printCalculations();
    }
    function addCalculation(){
        $dateNow = date("Y-m-d H:i:s");
        $userBrowser = getUserBrowser();
        $userIP = getUserIp();

        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $calcResult = $data->result;

        $csvLine = array($dateNow, $calcResult, $userBrowser, $userIP);
        return addLineToCSV('calc.csv', $csvLine);    
    }
    //Get user browser from User Agent - without using browscap.ini file
    function getUserBrowser(){
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $browserName = 'Unknown';
        if((preg_match('/MSIE/i',$userAgent) && !preg_match('/Opera/i',$userAgent)) || preg_match('/Trident/i',$userAgent)) {
            $browserName = 'Internet Explorer';
        }
        elseif(preg_match('/Firefox/i',$userAgent)) {
            $browserName = 'Mozilla Firefox';
        }
        elseif(preg_match('/Chrome/i',$userAgent)) {
            $browserName = 'Google Chrome';
        }
        elseif(preg_match('/Safari/i',$userAgent)) {
            $browserName = 'Apple Safari';
        }
        elseif(preg_match('/Opera/i',$userAgent)) {
            $browserName = 'Opera';
        }
        elseif(preg_match('/Netscape/i',$userAgent)) {
            $browserName = 'Netscape';
        }
        return $browserName;
    }
    function getUserIp(){
        $ip = 'Unknown';
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    function addLineToCSV($filename, $line){
        if ($file = fopen("{$filename}", "a")){
            if(fputcsv($file, $line)){
                return true;
            }
            fclose($file);
        }
        return false;
    }
    function printCalculations(){
        $calcArray = getArrayFromCsv('calc.csv');
        $calcArray = sortArray($calcArray, 1, SORT_DESC); //sort array by first column (date) desc
        echo '<table>';
        foreach($calcArray as $row){
            echo '<tr>';
            foreach($row as $value){ 
                echo "<td>{$value}</td>";
            }
            echo '</tr>';
        }
        echo '</table>';
    }
    function getArrayFromCsv($filename){
        $calcArray = array();
        if ($file = fopen("{$filename}", "r")){
            while ($data = fgetcsv($file, 1000, ",")){
                $calcArray[]= $data;		
            }
            fclose($file);
        }
        return $calcArray;
    }
    function sortArray($data, $index, $type){
        $ip = array();
        foreach($data as $key => $row){
            $ip[$key] = $row[$index];
        }
        array_multisort($ip, $type, $data);
        return $data;
    }
?>